import unittest

from . import hello


class HelloTest(unittest.TestCase):

    def test_foo(self):
        self.assertEqual(hello.foo(), 'foo')

    def test_bar(self):
        self.assertEqual(hello.bar(), 'bar')
